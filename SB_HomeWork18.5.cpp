﻿// SB_HomeWork18.5.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <stack>
using namespace std;


class steck
{
private:
	stack <int> MySteck;

public:
	void DelTopItem()
	{
		MySteck.pop();
	}

	void AddNewItem(int a)
	{
		MySteck.push(a);
	}

	void ShowTopItem()
	{
		cout << MySteck.top() << endl;
	}




};


int main()
{
	int size = 0;
	steck Arr;

	cout << "enter stack length" << endl;
	cin >> size;


	for (int i = 0; i < size; i++)
	{
		int a;
		cout << "Enter The Number" << endl;
		cin >> a;
		Arr.AddNewItem(a);
	}

	cout << "Let's See The Top Element: ";
	Arr.ShowTopItem();

	cout << "Now We Gonna Delete 2 Elements" << endl;
	Arr.DelTopItem();
	Arr.DelTopItem();

	cout << "Let's See The Top Element: ";
	Arr.ShowTopItem();

	system("pause");
}
